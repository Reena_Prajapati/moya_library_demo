//
//  APIManager.swift
//  MoyaLibraryDemo
//
//  Created by mac-00010 on 19/12/2020.
//

import Foundation
import Moya

enum APIManager {
    
    case post
    case comments
}

extension APIManager: TargetType {
   
    var baseURL: URL {
        return URL(string: "https://jsonplaceholder.typicode.com")!
    }
    
    var path: String {
        
        switch self {
        case .post:
            return "/posts"
        case .comments:
            return "/comments"
        }
    }
    
    var method: Moya.Method {
        
        switch self {
        case .post:
            return .get
        case .comments:
            return .get
        }
    }
    
    var sampleData: Data {
        return Data()
    }
    
    var task: Task {
        
        switch self {
        case .post:
            return .requestPlain
        case .comments:
            return .requestPlain
        }
    }
    
    var headers: [String : String]? {
        return ["Content-type": "application/json"]
    }
}

struct NetworkManager {
    
    static var provider = MoyaProvider<APIManager>()
    
    static func getPostList(completion: @escaping (([PostModel]) -> Void)) {
        
        provider.request(.post) { (result) in
            
            switch result {
            case .success(let response):
                
                do {
                    let dataResponse = try JSONDecoder().decode([PostModel].self, from: response.data)
                    completion(dataResponse)
                } catch {
                    print("")
                }
            
            case .failure(let error):
                print(error)
            }
        }
    }
}
