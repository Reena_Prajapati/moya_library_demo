//
//  ViewController.swift
//  MoyaLibraryDemo
//
//  Created by mac-00010 on 19/12/2020.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var tblPost: UITableView! {
        didSet {
            tblPost.register(UINib(nibName: "PostTblCell", bundle: nil), forCellReuseIdentifier: "PostTblCell")
        }
    }
    
    var arrPost = [PostModel]() {
        didSet {
            tblPost.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getPostFromServer()
    }
    
    private func getPostFromServer() {
        
        NetworkManager.getPostList { (postModel) in
            self.arrPost = postModel
        }
    }
}

extension ViewController: UITableViewDelegate, UITableViewDataSource {
   
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrPost.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: "PostTblCell") as? PostTblCell {
            
            cell.configure(arrPost[indexPath.row])
            return cell
        }
        
        return UITableViewCell()
    }
}
