//
//  PostTblCell.swift
//  MoyaLibraryDemo
//
//  Created by mac-00010 on 19/12/2020.
//

import UIKit

final class PostTblCell: UITableViewCell {

    @IBOutlet private weak var lblTitle: UILabel!
    @IBOutlet private weak var lblBody: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}

extension PostTblCell {
    
    func configure(_ postInfo: PostModel) {
        
        lblTitle.text = postInfo.title
        lblBody.text = postInfo.body
    }
}
