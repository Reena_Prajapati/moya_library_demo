//
//  AppDelegate.swift
//  MoyaLibraryDemo
//
//  Created by mac-00010 on 19/12/2020.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        window?.frame = UIScreen.main.bounds
        window?.rootViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "ViewController")
        window?.makeKeyAndVisible()
        return true
    }

}

